FROM node:10 AS build
WORKDIR /build

COPY package.json .
COPY babel.config.js .
COPY src/ src/
COPY public/ public/

RUN yarn
RUN yarn build

FROM nginx:1.15
COPY --from=build /build/dist /usr/share/nginx/html
